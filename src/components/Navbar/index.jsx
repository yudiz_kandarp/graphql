import React from 'react'
import { Nav, NavbarBrand, NavItem, NavLink } from 'reactstrap'

function Navbar() {
	return (
		<Navbar className='navbar' color='light' light expand='md'>
			<NavbarBrand href='/'>Elon Musk</NavbarBrand>
			<Nav className='ml-auto d-flex' navbar>
				<NavItem>
					<NavLink href='/create'>Users</NavLink>
				</NavItem>
			</Nav>
		</Navbar>
	)
}

export default Navbar
