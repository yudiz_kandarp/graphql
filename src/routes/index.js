import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import AddUser from 'pages/CreateUser'
import Home from 'pages/Home'

function AllRoutes() {
	return (
		<BrowserRouter>
			<Routes>
				<Route path='/'>
					<Route index element={<Home />} />
					<Route path='/create' element={<AddUser />} />
				</Route>
			</Routes>
		</BrowserRouter>
	)
}

export default index
