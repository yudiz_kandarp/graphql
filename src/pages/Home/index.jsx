import { useQuery } from '@apollo/client'
import React from 'react'
import Navbar from 'components/Navbar'
import { GET_ROCKETS } from 'graphql/user/user.query'

import './Home.scss'

function Home() {
	const { loading, error, data } = useQuery(GET_ROCKETS)

	if (error) return <div>Error!</div>

	return (
		<div className='home'>
			<div className='header'>
				<Navbar />
			</div>

			<div className='hero'>
				{loading ? (
					<h2>Loading...</h2>
				) : (
					data?.rockets?.map((item) => {
						return (
							<div
								key={item.id}
								className='card mt-3 mb-3'
								style={{ width: '18rem' }}
							>
								<div className='card-body'>
									<h3 className='card-title'>{item.name}</h3>
									<p className='card-text'>{item.description}</p>
									<p className='card-text'>
										active: {item.active ? 'Yes' : 'No'}
									</p>
									<p className='card-text'>Stages: {item.active}</p>
								</div>
							</div>
						)
					})
				)}
			</div>
		</div>
	)
}

export default Home
