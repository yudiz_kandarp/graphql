import React, { useState } from 'react'
import { useMutation } from '@apollo/client'
import Navbar from 'components/Navbar'
import { ADD_USER } from 'graphql/user/user.mutation'
import { useNavigate } from 'react-router-dom'
import './createUser.scss'

function AddUser() {
	const [user, setUser] = useState({
		name: '',
		rocket: '',
	})
	const navigate = useNavigate()

	const [addUser, { data }] = useMutation(ADD_USER)

	function createUser(e) {
		e.preventDefault()
		addUser({
			variables: {
				objects: [
					{
						name: user.name,
						rocket: user.rocket,
					},
				],
			},
		})
		navigate('/')
	}

	function handleChange(e) {
		setUser((prev) => ({ ...prev, [e.target.name]: e.target.value }))
	}

	return (
		<div className='home'>
			<div className='header'>
				<Navbar />
			</div>

			<div className='.form-data'>
				<h2 className='mt-3'>Add User</h2>
				<form className='mt-3' onSubmit={createUser}>
					<input type='text' name='name' onChange={handleChange} />
					<input type='text' name='rocket' onChange={handleChange} />
					<button className='add-btn' type='submit'>
						Submit
					</button>
				</form>
			</div>
		</div>
	)
}

export default AddUser
