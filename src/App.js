import React from 'react'
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client'
import AllRoutes from './routes'
import './App.scss'

function App() {
	const client = new ApolloClient({
		uri: 'https://api.spacex.land/graphql',
		cache: new InMemoryCache(),
	})

	return (
		<div className='main'>
			<div className='app'>
				<ApolloProvider client={client}>
					<AllRoutes />
				</ApolloProvider>
			</div>
		</div>
	)
}

export default App
