import { gql } from '@apollo/client'

export const ADD_USER = gql`
	mutation add($objects: [users_insert_input!]!) {
		add_user(objects: $objects) {
			returning {
				id
				name
				rocket
			}
		}
	}
`
